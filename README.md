## How to Rails

This repo is a work in progress designed to help you work your way through Rails.

The master branch is created by running the following command

`rails new the_blog -T --database=postgresql --skip-turbolinks --skip-spring`

Here is the breakdown of that command:

`rails new the_blog`

This command is using the Rails command line interface (`rails`) and calling the new command with an NAME argument.

This is the only command required to create a new Rails project. Everything created from here will default to Rails defaults.

`-T`

This flag tells Rails not to preload minitest. I like to use RSpec as a primary testing tool.

`--database=postgresql`

The default for Rails is sqlite3, so we tell it to use Postgres instead

`--skip-turbolinks --skip-spring`

These two libraries are designed to help Rails views load faster, but they tend to have a few problems that can make it into production, so I prefer to skip them.

`--api`

Adding this to the end of the `rails new` command will only load the libraries necessary to create an API, skipping views completely. This project didn't do that (we need the views), but it is an option.

## How to Use This Project

This project is divided into branches, each one dealing with a specific part of Rails. They are numbered sequentially based on the work performed and each README will be addressing the branch that it is in specifically. Follow along in the branch's README for a detailed walkthrough.

The first branch to go to from here is `1-testing-setup`
